import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types'

import { v4 as uuidv4 } from 'uuid';

const Formulario = ({ crearCita }) => {
	//le aplicamos destructuracion al arreglo de las citas que viene del componente padre
	//Crear el state de las citas
	const [cita, actualizarCita] = useState({
		namePet: '',
		ownersName: '',
		date: '',
		time: '',
		symptom: '',
	});

	const [error, actualizarError] = useState(false);

	//funcion que se ejecuta cada que el usuario escribe en algun input
	const actualizarState = (e) => {
		actualizarCita({
			...cita,
			[e.target.name]: e.target.value,
		});
	};

	//destructuring para extraer los valores
	const { namePet, ownersName, date, time, symptom } = cita;

	//cuando el usuario envia el formulario
	const submitCita = (e) => {
		e.preventDefault(); //previene que la peticion se tome como get y espera a que se ejecute por default al cargar la pagina

		//validacion
		if (
			namePet.trim() === '' ||
			ownersName.trim() === '' ||
			date.trim() === '' ||
			time.trim() === '' ||
			date.trim() === '' ||
			symptom.trim() === ''
		) {
			actualizarError(true);
			return;
		}

		//eliminar el mansaje de validacionsi es que existe
		actualizarError(false);

		// Asignar un ID cuando mue stras registros repetidos
		cita.id = uuidv4();

		//Crear la cita
		crearCita(cita);

		//vaciar el form
		//reutilizamos la funcion actualizarCita y dejamos vacios los strings
		actualizarCita({
			namePet: '',
			ownersName: '',
			date: '',
			time: '',
			symptom: '',
		});
	};

	return (
		<Fragment>
			<h2>Crear tu cita</h2>

			{error ? (
				<p className="alerta-error">Todos los campos son obligatorios</p>
			) : null}

			<form onSubmit={submitCita}>
				<label htmlFor="namePet">Nombre de tu mascota</label>
				<input
					id="namePet"
					type="text"
					name="namePet"
					className="u-full-width"
					placeholder="Nombre de tu mascota"
					onChange={actualizarState}
					value={namePet}
				/>

				<label>Nombre del dueño</label>
				<input
					type="text"
					className="u-full-width"
					name="ownersName" //nombre del propietario
					placeholder="Nombre del dueño"
					onChange={actualizarState}
					value={ownersName}
				/>

				<label>Fecha</label>
				<input
					type="date"
					className="u-full-width"
					name="date" //nombre del propietario
					onChange={actualizarState}
					value={date}
				/>

				<label>Hora</label>
				<input
					type="time"
					className="u-full-width"
					name="time" //nombre del propietario
					onChange={actualizarState}
					value={time}
				/>

				<label>Sintomas</label>
				<textarea
					className="u-full-width"
					name="symptom"
					onChange={actualizarState}
					value={symptom}
				></textarea>

				<button type="submit" className="u-full-width button-primary">
					Agregar cita
				</button>
			</form>
		</Fragment>
	);
};

// Ayudan a documentar el tipo de dato de cada variable que tiene un componente y manda error en la consola si no esta asi
Formulario.propTypes={
	crearCita: PropTypes.func.isRequired
}

export default Formulario;
