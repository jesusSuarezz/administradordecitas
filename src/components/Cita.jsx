import React from 'react';
import PropTypes from 'prop-types';


const Cita = ({ cita, deleteCita }) => (
	<div className="cita">
		<p>
			Mascota: <span>{cita.namePet}</span>
		</p>
		<p>
			Dueño: <span>{cita.ownersName}</span>
		</p>
		<p>
			Fecha: <span>{cita.date}</span>
		</p>
		<p>
			Hora: <span>{cita.namePet}</span>
		</p>
		<p>
			Sintomas: <span>{cita.symptom}</span>
		</p>
        <button 
        type="submit"
        className="button eliminar u-full-width"
        onClick={() => deleteCita(cita.id)}
        >
            Eliminar cita &times;
        </button>
	</div>
);

// Ayudan a documnetor el tipo de dato de cada variable que tiene un componente
Cita.propTypes={
    cita: PropTypes.object.isRequired,
    deleteCita: PropTypes.func.isRequired
}

export default Cita;
