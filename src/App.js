import React, {Fragment, useState, useEffect} from 'react';
import Formulario from './components/Formulario'
import Cita from './components/Cita'


function App() {
	//citas que se guardan en el localStarage del navegador
	let citasIniciales = JSON.parse(localStorage.getItem('citas')); //convierte el arreglo en un string para que lo acepte el localstorage
	//si no hay citas entonses citasIniciales es igual a un array vacio
	if (!citasIniciales) {
		citasIniciales = [];
	}

	//Arreglo de citas para iterarlas en el map
	const [citas, guardarCitas] = useState(citasIniciales);

	// useEffect para realizar ciertasoperaciones cuando el state cambia
	useEffect(() => {
		console.log('Documento listo ó se hizo un cambio en las citas');
		//citas que se guardan en el localStarage del navegador
		let citasIniciales = JSON.parse(localStorage.getItem('citas')); //convierte el arreglo en un string para que lo acepte el localstorage

		if (citasIniciales) {
			localStorage.setItem('citas', JSON.stringify(citas));
		} else {
			localStorage.setItem('citas', JSON.stringify([]));
		}
	}, [citas]); // Se ejecuta cada vez que citas tiene un cambio

	// Funcion que tome la citas actuales y agregue la nueva
	const crearCita = cita => {
		guardarCitas([
			...citas, //hacemos copia de la citas que ya hay
			cita, //guardamos la nueva cita
		]);
	};

	// Funcion para eliminar una cita por el ID
	const deleteCita = id => {
		console.log(id);
		const nuevasCitas = citas.filter((cita) => cita.id !== id);
		guardarCitas(nuevasCitas); //como nuevasCitas ya es un arreglo no necesita corchetes de nuevo
	};

	const titulo = citas.length ? 'Administra tus citas' : 'No hay citas';

	return (
		<Fragment>
			<h1>Formulario para tus citas caninas (Jesús Suárez)</h1>
			<div className="container">
				<div className="row">
					<div className="one-half column">
						<Formulario crearCita={crearCita} />
					</div>
					<div className="one-half column">
						<h1>{titulo}</h1>
						{citas.map((cita) => (
							<Cita
								key={cita.id}
								cita={cita} //se pasa el props cita y se itera la variable cita que esta en el map
								deleteCita={deleteCita}
							/>
						))}
					</div>
				</div>
			</div>
		</Fragment>
	);
}

export default App;
